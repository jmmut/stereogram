from arraydemo import surfdemo_show


def main2():
    try:
        import numpy as N
        import pygame.surfarray as surfarray
    except ImportError:
        raise ImportError("NumPy and Surfarray are required.")

    width = 1024
    height = 256
    mask_depth = 6
    mask = N.zeros((width, height, 1))
    # mask[:] = mask_depth/4.0
    width_margin = int(width / 3)
    height_margin = height / 4
    mask[width_margin:width - width_margin, height_margin:height / 2] = int(mask_depth * 0.75)
    shape_width = width - 2 * width_margin
    for i in range(0, shape_width):
        mask[width_margin + i, height / 2:height - height_margin] = int(mask_depth * i / shape_width)

    image = N.zeros((width, height, 3))
    image[:] = (50, 50, 50)
    image[width_margin:width - width_margin, height_margin:height - height_margin] = (255, 0, 0)
    surfdemo_show(image, 'rect')

    mask_depth_to_width = 1
    minimum_period = 40
    noise = N.zeros((mask_depth * mask_depth_to_width * 2 + minimum_period, 3))

    for h_i in range(0, height):
        for i in range(0, noise.shape[0]):
            noise[i, :] = (N.random.uniform(0, 255), N.random.uniform(0, 255), N.random.uniform(0, 255))
        print("line {}".format(h_i))
        for mask_level in range(0, mask_depth):
            mask_movement = mask_depth - mask_level
            # mask_movement *= 0.1
            mask_movement *= mask_depth_to_width
            mask_movement += minimum_period
            mask_movement = int(mask_movement)
            for w_i in range(0, width):
                if mask[w_i, h_i] == mask_level:
                    left_movement = w_i - mask_movement
                    if 0 <= left_movement < width:
                        image[left_movement, h_i] = noise[w_i % mask_movement, :]

                    right_movement = w_i + mask_movement
                    if 0 <= right_movement < width:
                        image[right_movement, h_i] = noise[w_i % mask_movement, :]

    surfdemo_show(image, 'rect')


def main():
    try:
        import numpy as N
        import pygame.surfarray as surfarray
    except ImportError:
        raise ImportError("NumPy and Surfarray are required.")

    width = 40
    image = N.zeros(width)
    mask = N.zeros(width)
    margin = 9
    mask_depth = 2
    mask_depth_to_width = 1
    required_palette_size = mask_depth * mask_depth_to_width
    palette = N.zeros(required_palette_size)
    for i in range(required_palette_size):
        palette[i] = i / float(required_palette_size)

    mask[margin:width - margin] = 1
    previous_mask = -1
    for w_i in range(width):
        movement_depth = mask_depth - mask[w_i]
        movement = movement_depth * mask_depth_to_width
        if w_i - movement < 0:
            image[w_i] = (required_palette_size + w_i - movement) % required_palette_size
        else:
            if previous_mask != mask[w_i]:
                image[w_i] = image[w_i - movement]
            else:
                image[w_i] =
                previous_mask = mask[w_i]

    print("palette: ")
    print(palette)
    print("mask: ")
    print(mask)
    print("image: ")
    print(image)


if __name__ == "__main__":
    main()
